#include <iostream>
using namespace std;

void append(unsigned char* stash, char* types, int& pos, int& size, int x) {
    unsigned char* temp = stash;
    temp += pos;

    *(int*)temp = x;
    pos += sizeof(int);

    types[size] = 'i';
    size++;
}

void append(unsigned char* stash, char* types, int& pos, int& size, char x) {
    unsigned char* temp = stash;
    temp += pos;

    *temp = x;
    pos += sizeof(char);

    types[size] = 'c';
    size++;
}

void append(unsigned char* stash, char* types, int& pos, int& size, float x) {
    unsigned char* temp = stash;
    temp += pos;

    *(float*)temp = x;
    pos += sizeof(float);

    types[size] = 'f';
    size++;
}

void print(unsigned char* stash, char* types, int size) {
    unsigned char* temp = stash;

    for (int i = 0; i < size; i++) {
        switch (types[i]) {
            case 'i':
                cout << *(int*)temp << " ";
                temp += sizeof(int);
                break;
            case 'c':
                cout << *temp << " ";
                temp += sizeof(char);
                break;
            case 'f':
                cout << *(float*)temp << " ";
                temp += sizeof(float);
                break;
        }
    }
    cout << endl;
}

int main() {
    // [42, 'A', 3.5]

    unsigned char* p = new unsigned char[10];
    char types[4];
    int pos = 0;
    int size = 0;

    append(p, types, pos, size, 42);
    append(p, types, pos, size, 'A');
    append(p, types, pos, size, 3.5f);
    append(p, types, pos, size, 'L');

    print(p, types, size);

    delete[] p;

    return 0;
}