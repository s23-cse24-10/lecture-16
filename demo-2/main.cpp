#include <iostream>
using namespace std;

void append(void **p, char* types, int& size, int x) {
    p[size] = new int(x);
    types[size] = 'i';
    size++;
}

void append(void **p, char* types, int& size, char x) {
    p[size] = new char(x);
    types[size] = 'c';
    size++;
}

void append(void **p, char* types, int& size, float x) {
    p[size] = new float(x);
    types[size] = 'f';
    size++;
}

void print(void **p, char* types, int size) {
    for (int i = 0; i < size; i++) {
        switch (types[i]) {
            case 'i':
                cout << *(int*)p[i] << " ";
                break;
            case 'c':
                cout << *(char*)p[i] << " ";
                break;
            case 'f':
                cout << *(float*)p[i] << " ";
                break;
        }
    }
    cout << endl;
}

int main() {
    // [42, 'A', 3.5]

    void **p = new void*[4];
    char types[4];
    int size = 0; 

    for (int i = 0; i < 4; i++) {
        p[i] = nullptr;
    }

    append(p, types, size, 42);
    append(p, types, size, 'A');
    append(p, types, size, 3.5f);

    print(p, types, size);

    for (int i = 0; i < size; i++) {
        switch (types[i]) {
            case 'i':
                delete[] (int*)p[i];
                break;
            case 'c':
                delete[] (char*)p[i];
                break;
            case 'f':
                delete[] (float*)p[i];
                break;
        }
    }

    delete[] p;

    return 0;
}